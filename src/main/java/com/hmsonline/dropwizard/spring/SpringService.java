// Copyright (c) 2012 Health Market Science, Inc.
package com.hmsonline.dropwizard.spring;

import java.text.MessageFormat;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import com.codahale.metrics.health.HealthCheck;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.hmsonline.dropwizard.spring.web.FilterConfiguration;
import com.hmsonline.dropwizard.spring.web.RestContextLoaderListener;
import com.hmsonline.dropwizard.spring.web.ServletConfiguration;
import com.hmsonline.dropwizard.spring.web.XmlRestWebApplicationContext;
import io.dropwizard.Application;
import io.dropwizard.cli.Cli;
import io.dropwizard.cli.ServerCommand;
import io.dropwizard.configuration.ConfigurationException;
import io.dropwizard.configuration.ConfigurationFactory;
import io.dropwizard.configuration.ConfigurationFactoryFactory;
import io.dropwizard.configuration.ConfigurationSourceProvider;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.servlets.tasks.Task;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.util.JarLocation;
import java.io.File;
import java.io.IOException;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.Servlet;
import javax.validation.Validator;
import javax.ws.rs.Path;
import javax.ws.rs.ext.Provider;
import org.springframework.context.ConfigurableApplicationContext;

public class SpringService extends Application<SpringServiceConfiguration> {

    private static final Logger LOG = LoggerFactory.getLogger(SpringService.class);

    public static void main(String[] args) throws Exception {
        new SpringService().run(args);
    }

    @Override
    public String getName() {
        return "dropwizard-spring";
    }

    @Override
    public void initialize(Bootstrap<SpringServiceConfiguration> bootstrap) {
        // This is needed to avoid an exception when deserializing Json to an ArrayList<String>
        bootstrap.getObjectMapper().enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
    }

    @Override
    public void run(SpringServiceConfiguration configuration, Environment environment) throws ClassNotFoundException {
        SpringConfiguration config = configuration.getSpring();

        ClassPathXmlApplicationContext parentCtx = this.initSpringParent();

        Dropwizard dw = (Dropwizard) parentCtx.getBean("dropwizard");
        dw.setConfiguration(configuration);
        dw.setEnvironment(environment);
        
        parentCtx.getBeanFactory().registerSingleton("objectMapper", environment.getObjectMapper());

        ApplicationContext appCtx = initSpring(config, parentCtx);
        loadResourceBeans(config.getResources(), appCtx, environment);
        loadHealthCheckBeans(config.getHealthChecks(), appCtx, environment);
        loadManagedBeans(config.getManaged(), appCtx, environment);
        loadLifeCycleBeans(config.getLifeCycles(), appCtx, environment);
        loadJerseyProviders(config.getJerseyProviders(), appCtx, environment);
        loadTasks(config.getTasks(), appCtx, environment);

        // Load filter or listeners for WebApplicationContext.
        if (appCtx instanceof XmlRestWebApplicationContext) {
            loadWebConfigs(environment, config, appCtx);
        }

        enableJerseyFeatures(config.getEnabledJerseyFeatures(), environment);
        disableJerseyFeatures(config.getDisabledJerseyFeatures(), environment);
    }

    /**
     * Load filter, servlets or listeners for WebApplicationContext.
     */
    private void loadWebConfigs(Environment environment, SpringConfiguration config, ApplicationContext appCtx) throws ClassNotFoundException {
        // Load filters.
        loadFilters(config.getFilters(), environment);

        // Load servlet listener.
        environment.servlets().addServletListeners(new RestContextLoaderListener((XmlRestWebApplicationContext) appCtx));

        // Load servlets.
        loadServlets(config.getServlets(), environment);
    }

    /**
     * Load all filters.
     */
    @SuppressWarnings("unchecked")
    private void loadFilters(Map<String, FilterConfiguration> filters, Environment environment) throws ClassNotFoundException {
        if (filters != null) {
            for (Map.Entry<String, FilterConfiguration> filterEntry : filters.entrySet()) {
                FilterConfiguration filter = filterEntry.getValue();

                // Create filter holder
                FilterHolder filterHolder = new FilterHolder((Class<? extends Filter>) Class.forName(filter.getClazz()));

                // Set name of filter
                filterHolder.setName(filterEntry.getKey());

                // Set params
                if (filter.getParam() != null) {
                    for (Map.Entry<String, String> entry : filter.getParam().entrySet()) {
                        filterHolder.setInitParameter(entry.getKey(), entry.getValue());
                    }
                }

                // Add filter
                environment.getApplicationContext().addFilter(filterHolder, filter.getUrl(), EnumSet.of(DispatcherType.REQUEST));
            }
        }
    }

    /**
     * Load all servlets.
     */
    @SuppressWarnings("unchecked")
    private void loadServlets(Map<String, ServletConfiguration> servlets, Environment environment) throws ClassNotFoundException {
        if (servlets != null) {
            for (Map.Entry<String, ServletConfiguration> servletEntry : servlets.entrySet()) {
                ServletConfiguration servlet = servletEntry.getValue();

                // Create servlet holder
                ServletHolder servletHolder = new ServletHolder((Class<? extends Servlet>) Class.forName(servlet.getClazz()));

                // Set name of servlet
                servletHolder.setName(servletEntry.getKey());

                // Set params
                if (servlet.getParam() != null) {
                    for (Map.Entry<String, String> entry : servlet.getParam().entrySet()) {
                        servletHolder.setInitParameter(entry.getKey(), entry.getValue());
                    }
                }

                // Add servlet
                environment.getApplicationContext().addServlet(servletHolder, servlet.getUrl());
            }
        }
    }

    private void loadResourceBeans(List<String> resources, ApplicationContext ctx, Environment env) {
        if (resources != null) {
            for (String resource : resources) {
                try {
                    env.jersey().register(ctx.getBean(resource));
                } catch (NoSuchBeanDefinitionException nsbde) {
                    logNoSuchBeanDefinitionException(nsbde);
                }
            }
        } else {
            LOG.info("No resource beans listed in config so scanning for all beans with @javax.ws.rs.Path annotation");
            for (final Object bean : ctx.getBeansWithAnnotation(Path.class).values()) {
                env.jersey().register(bean);
            }
        }

    }

    private void loadHealthCheckBeans(List<String> healthChecks, ApplicationContext ctx, Environment env) {
        if (healthChecks != null) {
            for (String healthCheck : healthChecks) {
                try {
                    HealthCheck healthCheckBean = (HealthCheck) ctx.getBean(healthCheck);
                    env.healthChecks().register(healthCheck, healthCheckBean);
                } catch (NoSuchBeanDefinitionException nsbde) {
                    logNoSuchBeanDefinitionException(nsbde);
                }
            }
        } else {
            LOG.info("No health check beans listed in config so scanning for all beans that implement com.codahale.metrics.health.HealthCheck");
            for (final Map.Entry<String,HealthCheck> entry : ctx.getBeansOfType(HealthCheck.class).entrySet()) {
                env.healthChecks().register(entry.getKey(), entry.getValue());
            }
        }
    }

    private void loadManagedBeans(List<String> manageds, ApplicationContext ctx, Environment env) {
        if (manageds != null) {
            for (String managed : manageds) {
                try {
                    env.getApplicationContext().manage(ctx.getBean(managed));
                } catch (NoSuchBeanDefinitionException nsbde) {
                    logNoSuchBeanDefinitionException(nsbde);
                }
            }
        }
    }

    private void loadLifeCycleBeans(List<String> lifeCycles, ApplicationContext ctx, Environment env) {
        if (lifeCycles != null) {
            for (String lifeCycle : lifeCycles) {
                try {
                    env.getApplicationContext().manage(ctx.getBean(lifeCycle));
                } catch (NoSuchBeanDefinitionException nsbde) {
                    logNoSuchBeanDefinitionException(nsbde);
                }
            }
        }
    }

    private void loadJerseyProviders(List<String> providers, ApplicationContext ctx, Environment env) {
        if (providers != null) {
            for (String provider : providers) {
                try {
                    env.jersey().register(ctx.getBean(provider));
                } catch (NoSuchBeanDefinitionException nsbde) {
                    logNoSuchBeanDefinitionException(nsbde);
                }
            }
        } else {
            LOG.info("No jersey provider beans listed in config so scanning for all beans with annotation @javax.ws.rs.ext.Provider");
            for (final Object bean : ctx.getBeansWithAnnotation(Provider.class).values()) {
                env.jersey().register(bean);
            }
        }
    }

    private void loadTasks(List<String> tasks, ApplicationContext ctx, Environment env) {
        if (tasks != null) {
            for (String task : tasks) {
                try {
                    env.admin().addTask((Task) ctx.getBean(task));
                } catch (NoSuchBeanDefinitionException nsbde) {
                    logNoSuchBeanDefinitionException(nsbde);
                }
            }
        } else {
            LOG.info("No tasks beans listed in config so scanning for all beans that implement io.dropwizard.servlets.tasks.Task");
            for (final Map.Entry<String,Task> entry : ctx.getBeansOfType(Task.class).entrySet()) {
                env.admin().addTask(entry.getValue());
            }
        }
    }

    private void enableJerseyFeatures(List<String> features, Environment env) {
        if (features != null) {
            for (String feature : features) {
                env.jersey().enable(feature);
            }
        }
    }

    private void disableJerseyFeatures(List<String> features, Environment env) {
        if (features != null) {
            for (String feature : features) {
                env.jersey().disable(feature);
            }
        }
    }

    private ClassPathXmlApplicationContext initSpringParent() {
        ClassPathXmlApplicationContext parent = new ClassPathXmlApplicationContext(
                new String[]{"dropwizardSpringApplicationContext.xml"}, true);
        
        return parent;
    }

    private ApplicationContext initSpring(SpringConfiguration config, ApplicationContext parent) {
        ConfigurableApplicationContext appCtx = null;
        // Get Application Context Type
        String ctxType = config.getAppContextType();
        // Get Config Location Type.
        String cfgLocationType = config.getConfigLocationsType();
        String[] configLocations = config.getConfigLocations().toArray(new String[config.getConfigLocations().size()]);

        if (SpringConfiguration.WEB_APPLICATION_CONTEXT.equals(ctxType)) {
            // Create Web Application Context.
            appCtx = new XmlRestWebApplicationContext(configLocations, cfgLocationType, false, parent);

        } else if (SpringConfiguration.APPLICATION_CONTEXT.equals(ctxType)) {

            // Create Application Context.
            if (SpringConfiguration.FILE_CONFIG.equals(cfgLocationType)) {
                appCtx = new FileSystemXmlApplicationContext(configLocations, false, parent);
            } else if (SpringConfiguration.CLASSPATH_CONFIG.equals(cfgLocationType)) {
                appCtx = new ClassPathXmlApplicationContext(configLocations, false, parent);
            } else {
                throw new IllegalArgumentException(MessageFormat.format("Configuration Error: configLocationsType must be either \"{0}\" or \"{1}\"", SpringConfiguration.FILE_CONFIG, SpringConfiguration.CLASSPATH_CONFIG));
            }
        } else {
            throw new IllegalArgumentException(MessageFormat.format("Configuration Error: appContextType must be either \"{0}\" or \"{1}\"", SpringConfiguration.WEB_APPLICATION_CONTEXT, SpringConfiguration.APPLICATION_CONTEXT));
        }
        if (config.getProfiles() != null) {
            appCtx.getEnvironment().setActiveProfiles(config.getProfiles().toArray(new String[0]));
        }
        appCtx.refresh();
        return appCtx;
    }

    private void logNoSuchBeanDefinitionException(NoSuchBeanDefinitionException nsbde) {
        if (LOG.isWarnEnabled()) {
            LOG.warn("Skipping missing Spring bean: ", nsbde);
        }
    }

    protected SpringServiceConfiguration parseFromClasspath(final String path) throws IOException {
        final ObjectMapper objectMapper = Jackson.newObjectMapper();
        final YAMLFactory parser = new YAMLFactory();
        return objectMapper.readValue(parser.createParser(getClass().getClassLoader().getResourceAsStream(path)), SpringServiceConfiguration.class);
    }
    
    protected void runWithClasspathConfig(final String path) throws Exception {
        run(parseFromClasspath(path));
    }

    protected void run(final SpringServiceConfiguration config) throws Exception {

        final Bootstrap<SpringServiceConfiguration> bootstrap = new Bootstrap<SpringServiceConfiguration>(this) {

            @Override
            public ConfigurationFactoryFactory<SpringServiceConfiguration> getConfigurationFactoryFactory() {
                return new ConfigurationFactoryFactory<SpringServiceConfiguration>() {

                    @Override
                    public ConfigurationFactory<SpringServiceConfiguration> create(Class<SpringServiceConfiguration> type, Validator vldtr, ObjectMapper om, String string) {
                        return new ConfigurationFactory<SpringServiceConfiguration>(SpringServiceConfiguration.class, null, new ObjectMapper(), "") {

                            @Override
                            public SpringServiceConfiguration build() throws IOException, ConfigurationException {
                                return config;
                            }

                            @Override
                            public SpringServiceConfiguration build(File file) throws IOException, ConfigurationException {
                                return config;
                            }

                            @Override
                            public SpringServiceConfiguration build(ConfigurationSourceProvider provider, String path) throws IOException, ConfigurationException {
                                return config;
                            }

                        };
                    }

                };
            }

        };
        bootstrap.addCommand(new ServerCommand<>(this));
        final Cli cli = new Cli(new JarLocation(getClass()), bootstrap, System.out, System.err);
        if (!cli.run(new String[]{"server"})) {
            // only exit if there's an error running the command
            System.exit(1);
        }
    }
}
